import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientHandler implements Runnable {

    Socket socket;
    Server server;
    BufferedReader in;
    PrintWriter out;
    String nickname;
    String textColor;
    boolean hasName;
    boolean hasClient;

    public ClientHandler(Socket socket, Server server, String textColor) throws IOException {
        this.socket = socket;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(socket.getOutputStream(), true);
        this.server = server;
        this.textColor = textColor;
        hasClient = true;
    }

    @Override
    public void run() {
        try {
            while(!hasName) {
                out.println(TextColor.RESET.getDescription() + "What's your name? (it can't contain spaces)");//"Qual é o teu nickname? (não pode conter espaços)");
                String name = in.readLine();
                if(name.contains(" ")) {
                    out.println(TextColor.RESET.getDescription() + "This nickname contains spaces you dumbass.");//"Este nickname contém espaços seu burro");
                } else if(server.existsNickname(name)) {
                    out.println(TextColor.RESET.getDescription() + "This nickname already exists.");//"Este nickname já existe");
                } else {
                    nickname = name;
                    hasName = true;
                    out.println(TextColor.RESET.getDescription() + "Welcome " + nickname);
                    server.notifyAllUsers(nickname + " entered the chat.", this);
                }
            }

            while(true) {
                String message;
                if(hasClient && (message = in.readLine()) != null) {
                    //message = checkAsneiras(message);
                    server.updateChat(message, nickname, textColor, this);
                } else {
                    server.notifyAllUsers(nickname + " left the chat", this);
                    server.getClients().remove(this);
                    in.close();
                    out.close();
                    socket.close();
                    break;
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String checkAsneiras(String line) {

        if(line.toLowerCase().contains("puta")) {
            return line.replaceAll("puta", "****");
        }
        return line;

    }
}
