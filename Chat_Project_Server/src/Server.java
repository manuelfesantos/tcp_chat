import com.sun.security.ntlm.Client;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {
    private ServerSocket serverSocket;
    private ExecutorService cachedPool;
    private final ArrayList<ClientHandler> clients;
    private boolean isPrivate;


    public Server() {
        try {
            serverSocket = new ServerSocket(6000);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        cachedPool = Executors.newCachedThreadPool();
        clients = new ArrayList<>();
    }

    public void awaitClients() {
        try {
            while (true) {

                Socket socket = serverSocket.accept();
                ClientHandler client = new ClientHandler(socket, this, TextColor.random());
                cachedPool.submit(client);
                clients.add(client);

            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void updateChat(String message, String nickname, String textColor, ClientHandler clientHandler) {
        String text = checkCommand(message, clientHandler);

        if (!isPrivate) {
            synchronized (clients) {
                for (ClientHandler clientH : clients) {
                    if (clientH.hashCode() != clientHandler.hashCode()) {
                        clientH.out.println("");
                        clientH.out.println("\u001B[0m" + nickname + ": " + text);
                    }
                }
            }
        }
    }

    public synchronized String checkCommand(String text, ClientHandler clientHandler) {
        if (text.length() > 0 && text.startsWith("/")) {
            String command = "";
            String commandTarget = "";
            if(text.contains(" ")) {
                command = text.split(" ")[0];
                commandTarget = text.split(" ")[1];
            } else {
                command = text;
            }
            if (command.equals("/private")) {
                for (ClientHandler client : clients) {
                    if (client.nickname.equals(commandTarget)) {
                        client.out.println("\u001B[0m" + clientHandler.nickname + " (private)" + ": " + clientHandler.textColor + text.substring(command.length() + commandTarget.length() + 2));
                        isPrivate = true;
                        return text;
                    }
                }
                clientHandler.out.println("\u001B[0m" + TextColor.RED_BOLD.getDescription() + "There is no one with that name!");
                isPrivate = true;
                return text;
            } else if (command.equals("/caps")) {
                isPrivate = false;
                return "\u001B[0m" + clientHandler.textColor + text.toUpperCase().substring(command.length()+1);
            } else if(command.equals("/underline")) {
                isPrivate = false;
                return "\u001B[0m" + "\u001B[4m" + clientHandler.textColor + text.substring(command.length()+1);
            } else if(command.equals("/italic")) {
                isPrivate = false;
                return "\u001B[0m" + "\u001B[3m" + clientHandler.textColor + text.substring(command.length()+1);
            } else if(command.equals("/bold")) {
                isPrivate = false;
                return "\u001B[0m" + "\u001B[1m" + clientHandler.textColor + text.substring(command.length()+1);
            } else if(command.contains("/list")) {
                isPrivate = true;
                    for (ClientHandler client : clients) {
                        if(client.hashCode() != clientHandler.hashCode()) {
                            clientHandler.out.println(client.textColor + "- " + client.nickname);
                        } else {
                            clientHandler.out.println(clientHandler.textColor + "- " + client.nickname + " (you)");
                        }
                    }
                return text;
            } else if(command.contains("/commands")) {
                clientHandler.out.println("\u001B[0m" + "/private + name + message: send a message to someone privately.");
                clientHandler.out.println("\u001B[0m" + "/caps + message: show your message in caps lock.");
                clientHandler.out.println("\u001B[0m" + "/underline + message: add an underline to your message.");
                clientHandler.out.println("\u001B[0m" + "/italic + message: show your message in italic.");
                clientHandler.out.println("\u001B[0m" + "/bold + message: show your message in bold.");
                clientHandler.out.println("\u001B[0m" + "/list: show all current clients on the server.");
                clientHandler.out.println("\u001B[0m" + "/listIP: show all clients' IP addresses.");
                clientHandler.out.println("\u001B[0m" + "/exit: exit chat.");
                isPrivate = true;
                return text;
            } else if(command.contains("/exit")) {
                clientHandler.hasClient = false;
                isPrivate = true;
                return text;
            } else if(command.contains("/listIP")) {
                for(ClientHandler client : clients) {
                    clientHandler.out.println(client.textColor + client.nickname + ": " + "\u001B[0m" + client.socket.getInetAddress());
                }
                isPrivate = true;
                return text;
            } else if(command.contains("/remove")) {
                for(ClientHandler client : clients) {
                    if(commandTarget.equals(client.nickname)) {
                            client.out.println("\u001B[0m" + clientHandler.nickname + " has removed you from the chat");
                            client.out.println("\u001B[0m" + "do you have any final words?");
                            notifyAllUsers("\u001B[0m" + client.nickname + " is to be removed from the chat", client);
                            client.hasClient = false;
                            isPrivate = true;
                        return text;
                    }
                }
                clientHandler.out.println("\u001B[0m" + TextColor.RED_BOLD.getDescription() + "There is no one with tht name!");
                isPrivate = true;
                return text;

            }
        }
        isPrivate = false;
        return "\u001B[0m" + clientHandler.textColor + text;
    }

    public boolean existsNickname(String name) {
        for(ClientHandler client : clients) {
            if(name.equals(client.nickname)) {
                return true;
            }
        }
        return false;
    }

    public void notifyAllUsers(String message, ClientHandler client) {

        for(ClientHandler clientHandler : clients) {
            if(client.hashCode() != clientHandler.hashCode()) {
                clientHandler.out.println(TextColor.RESET.getDescription() + message);
            }
        }
    }

    public ArrayList<ClientHandler> getClients() {
        return clients;
    }
}
