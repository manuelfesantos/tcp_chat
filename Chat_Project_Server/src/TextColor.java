public enum TextColor {

    RED("\u001B[31m"),
    GREEN("\u001B[32m"),
    YELLOW("\u001B[33m"),
    CYAN("\u001B[36m"),
    BLUE("\u001B[34m"),
    PURPLE("\u001B[35m"),
    RED_BOLD("\033[1;31m"),
    RESET("\u001B[0m");


    private String description;

    TextColor(String description) {
        this.description = description;
    }
    public String getDescription() {
        return description;
    }
    public static String random() {
        return values()[(int) (Math.random() * (values().length-2))].getDescription();
    }
}
