import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Receiver implements Runnable{

    Socket socket;
    BufferedReader socketIn;
    PrintWriter systemOut;


    public Receiver(Socket socket) {

        this.socket = socket;
        try {
            socketIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            systemOut = new PrintWriter(System.out, true);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public void run() {
        try {
            String message;
            while(true) {
                if((message = socketIn.readLine()) != null) {
                    systemOut.println(message);
                } else {
                    System.out.println("\u001B[0mserver disconnected");
                    socketIn.close();
                    socket.close();
                    System.exit(0);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
