import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Sender implements Runnable{

    Socket socket;
    BufferedReader systemIn;
    PrintWriter socketOut;
    Client client;

    public Sender(Socket socket, Client client) {
        this.client = client;
        this.socket = socket;
        systemIn = new BufferedReader(new InputStreamReader(System.in));
        try {
            socketOut = new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void run() {
        //socketOut.println(client.nickname);
        while(client.existsServer) {
            try {
                socketOut.println(systemIn.readLine());

            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
