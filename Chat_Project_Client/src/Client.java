import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Client {
    Socket socket;
    //String nickname;
    boolean existsServer;
    private ExecutorService fixedPool;

    public Client() {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        try {
            //System.out.println("What's your nickname?");
            //nickname = in.readLine();
            socket = new Socket("localhost", 6000);
            existsServer = true;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        fixedPool = Executors.newFixedThreadPool(2);

    }

    public void startConnection() {
        fixedPool.submit(new Receiver(socket));
        fixedPool.submit(new Sender(socket, this));
        //System.out.println("Created Threads");
    }
}
